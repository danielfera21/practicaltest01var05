package ro.pub.cs.systems.eim.practicaltest01var05;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static ro.pub.cs.systems.eim.practicaltest01var05.PracticalTest01Var05MainActivity.BROADCAST_ACTION;

public class PracticalTest01Var05SecondaryActivity extends AppCompatActivity {

    TextView gained;
    Button ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test01_var05_secondary);
        Intent intent = getIntent();
        String val01 = intent.getStringExtra("val01");
        String val02 = intent.getStringExtra("val02");
        String val03 = intent.getStringExtra("val03");
        int nr = intent.getIntExtra("nr", 0);
        gained = findViewById(R.id.gainedText);
        gained.setVisibility(View.GONE);
        ok = findViewById(R.id.ok);
        int win = 0;


        if ((val01.equals(val02) && val01.equals(val03))
                || (val01.equals("0") && val02.equals(val03))
                || (val02.equals("0") && val01.equals(val03))
                || (val03.equals("0") && val01.equals(val03))
                || (val01.equals("0") && val02.equals("0"))
                || (val01.equals("0") && val03.equals("0"))
                || (val03.equals("0") && val02.equals("0"))) {
            gained.setVisibility(View.VISIBLE);

            switch(nr) {
                case 0:
                    win = 100;
                    break;
                case 1:
                    win = 50;
                    break;
                case 2:
                    win = 10;
                    break;
                    default:break;
            }
            gained.setText("Gained " + win + "$");
            Data.getInstance().score += win;
            SharedPreferences.Editor editor = getSharedPreferences("preference", MODE_PRIVATE).edit();
            editor.putInt("score", Data.getInstance().score);
            editor.apply();
            if (Data.getInstance().score >= 300) {

            }
        }

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            registerReceiver(new PracticalTest01Var05MainActivity.MyReceiver(), intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void startMyService() {
        Intent intentService = new Intent(this, PracticalTest01Var05Service.class);
        Log.d("", "startMyService: ");
        startService(intentService);
    }
}
