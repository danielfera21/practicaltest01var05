package ro.pub.cs.systems.eim.practicaltest01var05;

public class Data {

    private Data() {
        score = 0;
    }

    private static Data instance = null;
    public static Data getInstance() {
        if (instance == null) {
            instance = new Data();
        }
        return instance;
    }

    public int score;
}
