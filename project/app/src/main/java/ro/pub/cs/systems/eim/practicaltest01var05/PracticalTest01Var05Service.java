package ro.pub.cs.systems.eim.practicaltest01var05;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.sql.Time;
import java.util.Calendar;

import static android.os.SystemClock.sleep;
import static ro.pub.cs.systems.eim.practicaltest01var05.PracticalTest01Var05MainActivity.BROADCAST_ACTION;

public class PracticalTest01Var05Service extends Service {
    public PracticalTest01Var05Service() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sleep(2000);
        Intent intent1 = new Intent();
        intent1.setAction(BROADCAST_ACTION);
        intent1.putExtra("message", "Victory: " + Calendar.getInstance().getTime().toString() + " - " + Data.getInstance().score);
        sendBroadcast(intent1);

        return Service.START_NOT_STICKY;
    }
}
