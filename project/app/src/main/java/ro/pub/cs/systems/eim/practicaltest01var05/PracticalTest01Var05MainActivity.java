package ro.pub.cs.systems.eim.practicaltest01var05;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class PracticalTest01Var05MainActivity extends AppCompatActivity {

    Button playButton;
    CheckBox ch01, ch02, ch03;
    EditText ed01, ed02, ed03;
    String[] values = {"0", "1", "2", "3"};
    public static final String BROADCAST_ACTION = "com.danielfera.colocviu1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_practical_test01_var05_main);
        playButton = findViewById(R.id.play_button);
        ch01 = findViewById(R.id.checkbox01);
        ch02 = findViewById(R.id.checkbox02);
        ch03 = findViewById(R.id.checkbox03);
        ed01 = findViewById(R.id.edit01);
        ed02 = findViewById(R.id.edit02);
        ed03 = findViewById(R.id.edit03);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val1 = values[Math.abs((new Random().nextInt()) % 4)];
                String val2 = values[Math.abs((new Random().nextInt()) % 4)];
                String val3 = values[Math.abs((new Random().nextInt()) % 4)];
                int nr_checked = 3;
                if (!ch01.isChecked()) {
                    ed01.setText(val1);
                    nr_checked--;
                } else {
                    val1 = ed01.getText().toString();
                }

                if (!ch02.isChecked()) {
                    ed02.setText(val2);
                    nr_checked--;
                }else {
                    val2 = ed02.getText().toString();
                }

                if (!ch03.isChecked()) {
                    ed03.setText(val3);
                    nr_checked--;
                }else {
                    val3 = ed03.getText().toString();
                }

                Log.d("", "onClick: " + nr_checked);

                Toast.makeText(PracticalTest01Var05MainActivity.this, val1 + ", " + val2 + ", " + val3, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(PracticalTest01Var05MainActivity.this, PracticalTest01Var05SecondaryActivity.class);
                intent.putExtra("val01", val1);
                intent.putExtra("val02", val2);
                intent.putExtra("val03", val3);
                intent.putExtra("nr", nr_checked);
                startActivity(intent);
            }
        });
        try
        {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BROADCAST_ACTION);
            registerReceiver(new MyReceiver(), intentFilter);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



    public static class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, intent.getStringExtra("message"), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences("preference", MODE_PRIVATE);
        Data.getInstance().score = prefs.getInt("score", 0);
        Toast.makeText(this, Data.getInstance().score + "$", Toast.LENGTH_LONG).show();
    }
}
